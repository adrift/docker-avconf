#!/bin/sh

set -e

echo "VIDEO_DEVICE: ${VIDEO_DEVICE}"
echo "OUTPUT_FORMAT: ${OUTPUT_FORMAT}"
echo "OUTPUT_FRAMERATE: ${OUTPUT_FRAMERATE}"
echo "RTMP_ENDPOINT: ${RTMP_ENDPOINT}"

avconv -ar 44100 -ac 2 -acodec pcm_s16le -f s16le -ac 2 -i "/dev/zero" -f video4linux2 -r "${OUTPUT_FRAMERATE}" -video_size "${OUTPUT_RESOLUTION}" -i "${VIDEO_DEVICE}" -vcodec copy -acodec aac -ab 128k -g 50 -strict experimental -vcodec libx264 -f "${OUTPUT_FORMAT}" -r "${OUTPUT_FRAMERATE}" "${RTMP_ENDPOINT}" 2>&1
